[![pipeline status](https://gitlab.com/csci3100-project/backend/badges/master/pipeline.svg)](https://gitlab.com/csci3100-project/backend/commits/master)
[![coverage report](https://gitlab.com/csci3100-project/backend/badges/master/coverage.svg)](https://gitlab.com/csci3100-project/backend/commits/master)

# Pre-reqs
- [Node.js](https://nodejs.org/en/) v8+ (I run on v9.6)
- NPM!!! Yarn unfortunately [does not](https://github.com/Microsoft/TypeScript-Node-Starter/issues/90) play well with this template
- [MongoDB](https://docs.mongodb.com/manual/installation/)

# Source
This repo is based on [Microsoft/TypeScript-Node-Starter](https://github.com/Microsoft/TypeScript-Node-Starter). Their version is developed for non-SPA websites. I converted it into this more REST-centered repo.

# Getting started
- Clone the repository
```
git clone <this repo's URL>
```
- Install dependencies
```
cd <project_name>
npm install
```

- Start your mongoDB server (make sure you have set up the correct permissions)
```
mongod
```

- Start the server (with auto-reload)
```
npm run watch
```
Then navigate to `http://localhost:3000`

## Installing third-party modules and fighting with TypeScript
Try the following strategy:

- See if it compiles -- if it works, it comes with its own type definitions
- `npm install --save-dev @types/jquery` to look for existing types on 
- Create file `src/types/<library.d.ts>` with one-line content: `declare module "<some-library>";` (and you'll import it like `import * as flash from "express-flash";`)

## Testing & Linting
`Linting`, as you probably know, checks your code for style conformance. For example, all codes in this project must use tab for indentation.

Unfortunately, upon every code push, your code will be tested and linted. A check/cross will be displayed right next to your commit, and GitLab will reject your push. (Just kidding.)

You are responsible for the cross (CI failure) caused. To make sure that everything goes well, run the following commands before you push:

- `npm run lint`
- `npm run test`