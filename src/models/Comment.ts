import mongoose from "mongoose";
import { pick } from "lodash";
import meanie from "@meanie/mongoose-to-json";

/**
 * Define the model of Comments
 * @var content - the data of the comment
 * @var submitter - the user who submitted this comment
 */
export type CommentModel = mongoose.Document & {
	content: String,
	submitter: any,
};

/**
 * Define the schema of Comments
 * @var content - the data of the comment
 * @var submitter - the user who submitted this comment
 */
const commentSchema = new mongoose.Schema({
	content: {type: String, required: true},
	submitter: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
}, <any>{ timestamps: true, strict: "throw" });
commentSchema.plugin(meanie);
commentSchema.index({submitter: 1});

export default commentSchema;