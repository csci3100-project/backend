import mongoose from "mongoose";
import { pick, pull } from "lodash";
import meanie from "@meanie/mongoose-to-json";
import sillyname from "sillyname";

/**
 * Define the model of User
 */
export type UserModel = mongoose.Document & {
	name: String,
	email: String,
	jwtClaim_iss: String,
	jwtClaim_sub: String,
	tags: Array<String>,
	followTag(tag: String): void,
	products: Array<String>,
	followProduct(tag: String): void,
};

/**
 * Define the schema of User
 */
const userSchema = new mongoose.Schema({
	name: {type: String, required: true},
	email: {type: String, required: false},
	jwtClaim_iss: {type: String, required: true},
	jwtClaim_sub: {type: String, required: true},
	tags: {type: [String], default: []},
	products: {type: [String], default: []},
}, <any>{ timestamps: true, strict: "throw" });
userSchema.plugin(meanie);
userSchema.index({jwtClaim_iss: 1, jwtClaim_sub: 1}, {unique: true});
userSchema.index({tags: 1});

/**
 * Find and return the user information only if the jwt data matches the user data in the database
 * @returns {User} - the result user data
 */
userSchema.statics.authenticateByJwt = async function(jwtClaim: any) {
	return await this.findOne({jwtClaim_iss: jwtClaim.iss, jwtClaim_sub: jwtClaim.sub}) || this.create({
		email: jwtClaim.email,
		name: jwtClaim.name || (jwtClaim.email ? jwtClaim.email.split("@")[0] : sillyname()), // unfortunately, Google does not conform to OIDC spec -- they sometimes skip the `profile` claim
		jwtClaim_iss: jwtClaim.iss,
		jwtClaim_sub: jwtClaim.sub,
	});
};

/**
 * Follow a tag
 * @param {string} - the tag's id
 */
userSchema.methods.followTag = function(tag: String) {
	this.tags.push(tag);
};

/**
 * Unfollow a tag
 * @param {string} - the tag's id
 */
userSchema.methods.unfollowTag = function(tag: String) {
	this.tags.pull(tag);
};

/**
 * Follow a product
 * @param {string} - the product's id
 */
userSchema.methods.followProduct = function(tag: String) {
	this.products.push(tag);
};

/**
 * Unfollow a product
 * @param {string} - the product's id
 */
userSchema.methods.unfollowProduct = function(tag: String) {
	this.products.pull(tag);
};

/** The interface of the user model */
export interface IUser extends mongoose.Model<UserModel> {
	authenticateByJwt(jwtClaim: any);
}

const User = mongoose.model("User", userSchema) as IUser;
User.on("index", function() {
	console.log("User index results", arguments);
});
export default User;