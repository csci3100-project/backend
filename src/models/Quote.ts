import mongoose from "mongoose";
import { pick } from "lodash";
import meanie from "@meanie/mongoose-to-json";

/**
 * Define the model of Quote
 */
export type QuoteModel = mongoose.Document & {
	price: String,
	comment: String,
	submitter: any,
	shop: any,
	date: Date
};

/**
 * Define the schema of Quote
 */
const quoteSchema = new mongoose.Schema({
	price: {type: String, required: true},
	comment: {type: String, required: false},
	submitter: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
	shop: { type: mongoose.Schema.Types.ObjectId, ref: "Shop", required: true },
	date: {type: Date, required: false},
}, <any>{ timestamps: true, strict: "throw" });
quoteSchema.plugin(meanie);
quoteSchema.index({submitter: 1});
quoteSchema.index({shop: 1});

export default quoteSchema;
