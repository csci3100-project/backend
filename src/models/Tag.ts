import { default as Shop, ShopModel } from "./Shop";
import { default as Product, ProductModel } from "./Product";

/**
 * Define the model of Tag
 */
export default class Tag {
	public shops: Array<ShopModel>;
	public products: Array<ProductModel>;
	public followers: Number;

	constructor() {}

	/**
	 * Find all the data related to this tag
	 * @param {string} - tag id
	 * @returns {Tag} - the tag object with shops, products, and followers
	 * @todo followers
	 */
	static async findOne(tagName: String) {
		const tag = new Tag();
		tag.shops = await Shop.find({tags: tagName});
		tag.products = await Product.find({tags: tagName});
		tag.followers = 42;
		return tag;
	}

	trimQuotes() {
		for (const product of this.products) {
			product.quotes = product.quotes.length > 0 ? [product.quotes[0]] : product.quotes;
		}
	}
}