import mongoose from "mongoose";
import { pick } from "lodash";
import { ObjectID } from "bson";
import Fuse from "fuse.js";

import { default as quoteSchema, QuoteModel } from "./Quote";
import { default as commentSchema, CommentModel } from "./Comment";

/**
 * Define the model of Product
 */
export type ProductModel = mongoose.Document & {
	name: String,
	ean: String,
	link?: String,
	spec?: any,
	status?: String,
	quotes: Array<QuoteModel>,
	comments: Array<CommentModel>,
	tags: Array<String>,
	photos: Array<String>,
};

/**
 * Define the schema of Product
 */
const productSchema = new mongoose.Schema({
	name: {type: String, required: true},
	ean: {type: String, required: true},
	link: {type: String, required: false},
	spec: {type: mongoose.Schema.Types.Mixed, required: false},
	status: {type: String, required: false},
	quotes: {type: [quoteSchema], default: []},
	comments: {type: [commentSchema], default: []},
	tags: {type: [String], default: []},
	photos: {type: [String], default: []},
}, <any>{ timestamps: true, strict: "throw", useNestedStrict: true, toJSON: {
	transform: function (doc, obj) {
		delete obj.__v;
		delete obj._id;
	},
}});
productSchema.index({ean: 1}, {unique: true});
productSchema.index({tags: 1});

/**
 * Search the db to find a product that fit the query EAN
 * @param {string} - The query EAN
 * @returns {IProduct} - the result
 */
productSchema.statics.findByEan = function(ean: String) {
	return this.findOne({ ean: ean });
};

/**
 * Find all the products which have been commented by a particular user
 * @param {string} - user id
 * @returns {array} - the product list
 */
productSchema.statics.commentsBy = async function(userId: string) {
	const products = await this.find({ "comments.submitter": userId });
	for (const product of products) {
		product.comments = product.comments.filter((comment) => {
			return comment.submitter == userId;
		});
	}
	return products;
};

/**
 * Find all the products which have been quoted by a particular user
 * @param {string} - user id
 * @returns {array} - the product list
 */
productSchema.statics.quotesBy = async function(userId: string) {
	const products = await this.find({ "quotes.submitter": userId });
	for (const product of products) {
		product.quotes = product.quotes.filter((quote) => {
			return quote.submitter == userId;
		});
	}
	return products;
};

/**
 * Find all the products which have been quoted for the same shop
 * @param {string} - shop id
 * @returns {array} - the product list
 */
productSchema.statics.quotesFor = async function(shopId: string) {
	const products = await this.find({ "quotes.shop": shopId });
	for (const product of products) {
		product.quotes = product.quotes.filter((quote) => {
			return quote.shop == shopId;
		});
	}
	return products;
};

/** The interface of the model */
export interface IProduct extends mongoose.Model<ProductModel> {
	findByEan(jwtClaim: any);
	commentsBy(userId: string);
	quotesBy(userId: string);
	quotesFor(shopId: string);
}

const Product = mongoose.model("Product", productSchema) as IProduct;
Product.on("index", function() {
	console.debug("Product index results", arguments);
});
export default Product;

/** refresh the fuse.js Database */
export let fuse: Fuse = undefined;
async function refreshFuseDb() {
	try {
		const products = await Product.find({});
		fuse = new Fuse(products, {
			tokenize: true,
			includeScore: true,
			threshold: 0.6,
			distance: 100,
			keys: [
				"name", "link", "spec", "comments", "tags",
			],
		});
		console.log("Finished loading/refreshing products db for fuse.js search engine");
	} catch (e) {
		console.error("Error while initializing fuse.js search engine", e);
	}
}
refreshFuseDb();

/** Whenever it saves, the fuse.js database will be updated */
productSchema.post("save", function(doc, next) {
	(<any>next)();
	refreshFuseDb();
});