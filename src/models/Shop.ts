import mongoose from "mongoose";
import { pick } from "lodash";
import meanie from "@meanie/mongoose-to-json";

/**
 * Define the model of Shop
 */
export type ShopModel = mongoose.Document & {
	name: String,
	chainName?: String,
	address: String,
	telephone?: String,
	openingHours?: Array<String>,
	website?: String,
	tags: Array<String>,
};

/**
 * Define the schema of Shop
 */
const shopSchema = new mongoose.Schema({
	name: {type: String, required: true},
	chainName: {type: String, required: false},
	address: {type: String, required: true},
	telephone: {type: String, required: false},
	openingHours: {type: [String], required: false, default: []},
	website: {type: String, required: false},
	tags: {type: [String], default: []},
}, <any>{ timestamps: true, strict: "throw" });
shopSchema.index({chainName: 1});
shopSchema.index({tags: 1});
shopSchema.plugin(meanie);

const Shop = mongoose.model<ShopModel>("Shop", shopSchema);
Shop.on("index", function() {
	console.debug("Shop index results", arguments);
});
export default Shop;
