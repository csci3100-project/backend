import { Router, Request, Response, NextFunction } from "express";
import { default as jwt, SecretCallback } from "express-jwt";
import jwksRsa from "jwks-rsa";

// import User from "../models/User";

export class UnknownJwtIssuerError extends Error {
	constructor() {
		super("Unknown JWT issuer");
		this.name = "UnknownJwtIssuerError";
	}
}

export class InvalidGoogleJwtError extends Error {
	constructor() {
		super("The Google token is not issued for BuyLa");
		this.name = "InvalidGoogleJwtError";
	}
}
export default function protect(options: any = {}) {
	const google = jwksRsa.expressJwtSecret({
		cache: true,
		rateLimit: true,
		jwksRequestsPerMinute: 5,
		jwksUri: "https://www.googleapis.com/oauth2/v3/certs",
	});

	const microsoft = jwksRsa.expressJwtSecret({
		cache: true,
		rateLimit: true,
		jwksRequestsPerMinute: 5,
		jwksUri: "https://login.microsoftonline.com/common/discovery/v2.0/keys",
	});

	const verifyOidcFunction = <SecretCallback> function(req: Request, header: any, payload: any, done: (err: any, secret?: string) => void): void {
		if ("accounts.google.com" === payload.iss) {
			if ("248918510154-j4respvpped3ag5mp1vh915fm6964lu6.apps.googleusercontent.com" !== payload.aud) {
				console.error("JWT aud mismatch", payload.aud);
				return done(new InvalidGoogleJwtError());
			}
			return google.apply(null, arguments);
		} else if ("https://csci3100.isaac.pw" === payload.iss) {
			return done(null, "qwertyuiopasdfghjklzxcvbnm123456");
		} else if ("7e4dbd20-e39e-4981-a8c1-edd24679f0f2" === payload.aud) {
			return microsoft.apply(null, arguments);
		} else {
			done(new UnknownJwtIssuerError());
		}
	};

	const router: Router = Router();
	router.use(jwt(Object.assign({
		// Dynamically provide a signing key based on the kid in the header and the singing keys provided by the JWKS endpoint.
		secret: verifyOidcFunction,

		// Validate the audience and the issuer.
		algorithms: ["RS256", "HS256"],
	}, options)));
	/* temporarily taking this down because not sure if all requests needs the user instance...
	router.use(async function(req: Request, res: Response, next: NextFunction) {
		if (req.user) {
			try {
				res.locals.user = req.user;
				req.user = await User.authenticateByJwt(req.user);
				next();
			} catch (e) {
				next(e);
			}
		}
	}); */
	router.use(function unknownJwtIssuerErrorCatcher(err, req, res, next) {
		if (err instanceof UnknownJwtIssuerError || err instanceof InvalidGoogleJwtError) {
			res.status(401);
		}
		next(err);
	});
	return router;
}