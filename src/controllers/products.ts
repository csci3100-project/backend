import { Response, Request as RequestOld, RequestHandler } from "express";
import User from "../models/User";
import Product from "../models/Product";
import mongoose from "mongoose";
import { startsWith } from "lodash";

interface Request extends RequestOld {
	files: any;
}
/**
 * Get a particular product by its EAN
 * @param {Request} - the request include EAN
 * @param {Response} - the response handler
 */
export const get: RequestHandler = async function (req: Request, res: Response) {
	const product = await Product.findByEan(req.params.productEan);
	res.send(product);
};

/**
 * List all the products
 * @param {Request} - the request
 * @param {Response} - the response handler
 */
export const list: RequestHandler = async function (req: Request, res: Response) {
	res.send(await Product.find({}));
};

/**
 * Update a product information
 * It first formats the data, then updates the database
 * @param {Request} - the request include EAN, the updated data
 * @param {Response} - the response handler
 */
export const update: RequestHandler = async function (req: Request, res: Response) {
	const user = await User.authenticateByJwt(req.user);
	const product = await Product.findByEan(req.params.productEan);
	if (!product) {
		res.status(404);
		throw new Error("EAN not found");
	}
	delete req.body.quotes;
	delete req.body.comments;
	if (!req.body.photos || !Array.isArray(req.body.photos)) {
		req.body.photos = product.photos;
	}
	if (req.files) {
		req.body.photos = req.body.photos.concat(req.files.map((file) => "/" + file.path.replace("\\", "/")));
	}
	Object.assign(product, req.body);
	await product.save();
	res.send(product);
};

/**
 * Create a product
 * It first formats the data, then insert a new document
 * @param {Request} - the request include EAN, the data to be uploaded
 * @param {Response} - the response handler
 */
export const create: RequestHandler = async function (req: Request, res: Response) {
	const user = await User.authenticateByJwt(req.user);
	delete req.body.quotes;
	delete req.body.comments;
	if (!req.body.photos || !Array.isArray(req.body.photos)) {
		req.body.photos = [];
	}
	if (req.files) {
		req.body.photos = req.body.photos.concat(req.files.map((file) => "/" + file.path.replace("\\", "/")));
	}
	try {
		const product = await Product.create(req.body);
		res.send(product);
	} catch (e) {
		if (e.name == "BulkWriteError" && startsWith(e.message, "E11000 ")) {
			res.status(400);
			throw new Error("Product with the given EAN already exists in the database");
		} else {
			throw e;
		}
	}
};

/**
 * Get a list of products which commneted by a particular user
 * @param {Request} - the request include the user id
 * @param {Response} - the response handler
 */
export const commentsByUser: RequestHandler = async function (req: Request, res: Response) {
	res.send(await Product.commentsBy(req.params.userId));
};

/**
 * Get a list of products which contains quote created by a particular user
 * @param {Request} - the request include the user id
 * @param {Response} - the response handler
 */
export const quotesByUser: RequestHandler = async function (req: Request, res: Response) {
	res.send(await Product.quotesBy(req.params.userId));
};

/**
 * Get a list of products which contains quotes from the same shop
 * @param {Request} - the request include the shop id
 * @param {Response} - the response handler
 */
export const quotesForShop: RequestHandler = async function (req: Request, res: Response) {
	res.send(await Product.quotesFor(req.params.shopId));
};