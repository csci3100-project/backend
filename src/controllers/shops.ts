import { Response, Request, RequestHandler } from "express";
import User from "../models/User";
import Shop from "../models/Shop";
import mongoose from "mongoose";

// const User = <IUser>mongoose.model("User");
// const Shop = mongoose.model("Shop");

/**
 * Get the shop information by shop id
 * @param {Request} - the request include shop id
 * @param {Response} - the response handler
 */
export const get: RequestHandler = async function (req: Request, res: Response) {
	const shop = await Shop.findById(req.params.shopId);
	res.send(shop);
};

/**
 * Get a list of shops
 * @param {Request} - the request. If it includes chain, it searchs inside the chain. Otherwise return all shops
 * @param {Response} - the response handler
 */
export const list: RequestHandler = async function (req: Request, res: Response) {
	let shops;
	if (req.query.chain && typeof req.query.chain === "string") {
		shops = await Shop.find({chainName: req.query.chain});
	} else {
		shops = await Shop.find({});
	}
	res.send(shops);
};

/**
 * Update the shop information
 * @param {Request} - the request include shop id, the updated data
 * @param {Response} - the response handler
 */
export const update: RequestHandler = async function (req: Request, res: Response) {
	const user = await User.authenticateByJwt(req.user);
	const shop = await Shop.findById(req.params.shopId);
	Object.assign(shop, req.body);
	await shop.save();
	res.send(shop);
};

/**
 * Create a new shop
 * @param {Request} - the request include the data of the new shop
 * @param {Response} - the response handler
 */
export const create: RequestHandler = async function (req: Request, res: Response) {
	const user = await User.authenticateByJwt(req.user);
	const shop = await Shop.create(req.body);
	res.send(shop);
};