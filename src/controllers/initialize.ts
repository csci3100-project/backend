import { Response, Request, RequestHandler } from "express";
import parse from "csv-parse/lib/sync";
import fs from "fs";
import { default as User, UserModel } from "../models/User";
import { default as Shop, ShopModel } from "../models/Shop";
import Product from "../models/Product";

export const confirm: RequestHandler = async function (req: Request, res: Response) {
	res.send("<!DOCTYPE HTML><form method=POST><input type='submit'></form>");
};

/**
 * Initialze the data by importing the data from google.csv
 * @param {Request} - the request
 * @param {Response} - the response handler
 */
export const action: RequestHandler = async function (req: Request, res: Response) {
	await User.remove({});
	await Product.remove({});
	await Shop.remove({});

	/** Create a dummy user */
	const user: UserModel = await User.create({
		name: "John Doe",
		email: "johndoe@example.com",
		jwtClaim_sub: "demo_only@csci3100.local",
		jwtClaim_iss: "Isaac" + Math.floor(123456789 * Math.random()),
	});

	const csv = fs.readFileSync("import/google.csv", {encoding: "utf-8"});

	let records = parse(csv);

	/** Fetching the data row by row */
	records = records.map(async (record) => {
		const shop1 = await Shop.create({
			name: record[7],
			chainName: record[8],
			address: record[9],
		});

		const quotes = [{
			price: record[11].replace("$", ""),
			date: new Date(record[12]),
			updatedAt: new Date(record[12]),
			submitter: user._id,
			shop: shop1._id,
		}];

		if (record[18]) {
			const shop2 = await Shop.create({
				name: record[14],
				chainName: record[15],
				address: record[16],
			});

			quotes.push({
				price: record[18].replace("$", ""),
				date: new Date(record[19]),
				updatedAt: new Date(record[19]),
				submitter: user._id,
				shop: shop2._id,
			});
		}

		return {
			name: record[2],
			ean: record[1],
			link: record[3],
			quotes: quotes,
			comments: [{
				content: record[6],
				submitter: user._id,
			}],
			tags: record[4].split(",").map((tag) => tag.trim()),
			photos: [record[5]],
		};
	});

	records = await Promise.all(records);

	await Product.insertMany(records);

	// console.log(records);

	res.send("Success");
};