import { Response, Request, RequestHandler, NextFunction } from "express";
import User from "../models/User";
import mongoose from "mongoose";

// const User = <IUser>mongoose.model("User");

/**
 * Get the user information by its id
 * @param {Request} - the request include user id
 * @param {Response} - the response handler
 */
export const get: RequestHandler = async function (req: Request, res: Response) {
	const user = await User.findById(req.params.userId);
	res.send(user);
};

/**
 * Update the user's information
 * @param {Request} - the request include JWT, user id, the updated data
 * @param {Response} - the response handler
 */
export const update: RequestHandler = async function (req: Request, res: Response, next: NextFunction) {
	const user = await User.authenticateByJwt(req.user);
	if (req.params.userId !== user.id) {
		res.send(403);
		console.error(`User ${user.id} tried to modify User instance ${req.params.userId}`);
		return next(new Error("You can only modify your own User instance"));
	}
	delete req.body.jwtClaim_iss;
	delete req.body.jwtClaim_sub;
	Object.assign(user, req.body);
	await user.save();
	res.send(user);
};

/**
 * Follow a tag
 * @param {Request} - the request include JWT, the tag id
 * @param {Response} - the response handler
 */
export const followTag: RequestHandler = async function (req: Request, res: Response) {
	const user = await User.authenticateByJwt(req.user);
	if (user.id !== req.params.userId) {
		res.status(403);
		throw new Error("You can only follow 'from' your own account");
	}
	await user.followTag(req.params.tagId);
	await user.save();
	res.status(201).end();
};

/**
 * Unfollow a tag
 * @param {Request} - the request include JWT, the tag id
 * @param {Response} - the response handler
 */
export const unfollowTag: RequestHandler = async function (req: Request, res: Response) {
	const user = await User.authenticateByJwt(req.user);
	if (user.id !== req.params.userId) {
		res.status(403);
		throw new Error("You can only unfollow 'from' your own account");
	}
	await user.unfollowTag(req.params.tagId);
	await user.save();
	res.status(204).end();
};

/**
 * Follow(bookmark) a product
 * @param {Request} - the request include JWT, the product id
 * @param {Response} - the response handler
 */
export const followProduct: RequestHandler = async function (req: Request, res: Response) {
	const user = await User.authenticateByJwt(req.user);
	if (user.id !== req.params.userId) {
		res.status(403);
		throw new Error("You can only follow 'from' your own account");
	}
	await user.followProduct(req.params.productId);
	await user.save();
	res.status(201).end();
};

/**
 * Unfollow a product
 * @param {Request} - the request include JWT, the product id
 * @param {Response} - the response handler
 */
export const unfollowProduct: RequestHandler = async function (req: Request, res: Response) {
	const user = await User.authenticateByJwt(req.user);
	if (user.id !== req.params.userId) {
		res.status(403);
		throw new Error("You can only unfollow 'from' your own account");
	}
	await user.unfollowProduct(req.params.productId);
	await user.save();
	res.status(204).end();
};

/**
 * Undirect the route of /me to /:userid
 * @param {Request} - the request include JWT
 * @param {Response} - the response handler
 */
export const redirMe: RequestHandler = async function (req: Request, res: Response) {
	const user = await User.authenticateByJwt(req.user);
	const url: string = req.originalUrl.replace("/me", `/users/${user.id}`);
	res.redirect(307, url);
};