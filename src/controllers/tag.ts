import { Response, Request, RequestHandler } from "express";
import Tag from "../models/Tag";
import Product from "../models/Product";

/**
 * Return the specific tag information
 * @param {Request} - the request include the tag id
 * @param {Response} - the response handler
 */
export const show: RequestHandler = async function (req: Request, res: Response) {
	const tag = await Tag.findOne(req.params.tagId);
	res.send(tag);
};

/**
 * Show all the trending products
 * @param {Request} - the request
 * @param {Response} - the response handler
 */
export const userHome: RequestHandler = async function (req: Request, res: Response) {
	let products = await Product.find({});
	products = products.filter((product) => product.quotes.length > 0);
	for (const product of products) {
		product.quotes = [product.quotes[0]];
	}
	res.send(products);
};