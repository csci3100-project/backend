import { Response, Request as RequestOld, RequestHandler } from "express";
import Product, { ProductModel, fuse } from "../models/Product";
import vision from "@google-cloud/vision";
import { unlinkSync } from "fs";

interface Request extends RequestOld {
	file: any;
}

/** Bind the key for Google Vision API */
const googleClient = new vision.ImageAnnotatorClient({
	keyFilename: "./gcp_credentials.json",
});

/**
 * Search the products by keywords
 * @param {Request} - the request include the keyword
 * @param {Response} - the response handler
 * @requires fuse
 */
export const searchByText: RequestHandler = async function (req: Request, res: Response) {
	if (!req.query.keyword) {
		res.status(400);
		throw new Error("Please use `keyword` GET parameter for search");
	}
	const results = fuse.search(req.query.keyword);
	res.send(results.map(mapSearchResultForClient));
};

const mapSearchResultForClient = (result) => Object.assign({score: result.score}, result.item.toJSON());

/** Reduce searching label to a string  */
function reduceLabels(arr) {
	return arr.reduce((acc, curr) => {
		return acc + " " + curr;
	});
}


/**
 * Search the products by an image
 * @param {Request} - the request include the file
 * @param {Response} - the response handler
 * @requires googleClient
 */
export const searchByImage: RequestHandler = async function (req: Request, res: Response) {
	if (!req.file) {
		res.status(400);
		throw new Error("You must upload an image for search");
	}
	try {
		const request = {
			image: {
				source: {
					filename: req.file.path,
				},
			},
			imageContext: {
				webDetectionParams: {
					includeGeoResults: true,
				},
			},
		};

		// Performs label detection on the image file
		const response = await googleClient.webDetection(request);

		let labels;
		const labels_more = response[0].webDetection.webEntities.map((entity) => entity.description);
		if (response[0].webDetection.bestGuessLabels) {
			labels = response[0].webDetection.bestGuessLabels.map((label) => label.label);
		} else {
			labels = labels_more;
		}

		console.log("keywords computed", labels, labels_more);

		let results = fuse.search(reduceLabels(labels));

		if (!results) {
			results = fuse.search(reduceLabels(labels_more));
		}

		res.send(results.map(mapSearchResultForClient));
	} catch (e) {
		unlinkSync(req.file.path);
		throw e;
	}
};