import { Response, Request, RequestHandler } from "express";
import User from "../models/User";
import Product from "../models/Product";
import mongoose from "mongoose";
import Shop from "../models/Shop";

export default class ProductChildHandler {

	/**
	 * Create a shared handler for Comments and Quotes
	 * @param modelName model name in plural form
	 */
	constructor(private modelName: string) {}

	/**
	 * Create a new child record under product
	 * @param {Request} - the data to be uploaded
	 * @param {Response} - the response handler
	 */
	create: RequestHandler = async (req: Request, res: Response) => {
		const user = await User.authenticateByJwt(req.user);
		const product = await Product.findByEan(req.params.productEan);
		req.body.submitter = user._id;
		const index = product[this.modelName].push(req.body);
		await product.save();
		res.send(product[this.modelName][index - 1]);
	};

	/**
	 * Update a new child record under product
	 * @param {Request} - the data to be uploaded
	 * @param {Response} - the response handler
	 */
	update: RequestHandler = async (req: Request, res: Response) => {
		const user = await User.authenticateByJwt(req.user);
		const product = await Product.findByEan(req.params.productEan);
		if (!product) {
			res.status(404);
			throw new Error("EAN not found");
		}

		const quote = product[this.modelName].id(req.params.quoteId);
		if (!quote) {
			res.status(404);
			throw new Error(this.modelName + " not found");
		}
		if (quote.submitter != user.id) {
			res.status(403);
			throw new Error(`You can only change ${this.modelName} you submitted`);
		}
		delete req.body.submitter;
		Object.assign(quote, req.body);
		await product.save();
		res.send(quote);
	};
}