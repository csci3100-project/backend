import { Router, Request, Response, NextFunction } from "express";
import * as express from "express";
import multer from "multer";
import { v4 } from "uuid";
import { last } from "lodash";

import jwt from "../lib/jwt";

import * as UsersControllers from "./users";
import * as ShopsControllers from "./shops";
import * as ProductsControllers from "./products";
import * as QuotesControllers from "./quotes";
import * as CommentsControllers from "./comments";
import * as TagControllers from "./tag";
import * as SearchControllers from "./search";
import * as InitializeControllers from "./initialize";
import JwtTestController from "./jwt-test";

/**
 * Create and initialize the required objects
 */
const router: Router = Router();
const jwtMiddleware = jwt();
const jwtMiddleware2 = jwt({credentialsRequired: false});
const upload = multer({
	storage: multer.diskStorage({
		destination: "uploads/",
		filename: function (req, file, cb) {
			cb(null, v4() + "." + last(file.originalname.split(".")));
		},
	}),
});

/**
 * Bind the route to specific controller's handler
 * @requires express.Router
 */
router.post("/products/:productEan/quotes", jwtMiddleware, QuotesControllers.create);
router.patch("/products/:productEan/quotes/:quoteId", jwtMiddleware, QuotesControllers.update);

router.post("/products/:productEan/comments", jwtMiddleware, CommentsControllers.create);
router.patch("/products/:productEan/comments/:quoteId", jwtMiddleware, CommentsControllers.update);

router.get("/users/:userId/comments", ProductsControllers.commentsByUser);
router.get("/users/:userId/quotes", ProductsControllers.quotesByUser);
router.get("/shops/:shopId/quotes", ProductsControllers.quotesForShop);

router.get("/products/:productEan", ProductsControllers.get);
router.patch("/products/:productEan", jwtMiddleware, upload.array("photos"), ProductsControllers.update);

router.get("/products", ProductsControllers.list);
router.post("/products", jwtMiddleware, upload.array("photos"), ProductsControllers.create);

router.post("/users/:userId/tags/:tagId", jwtMiddleware,  UsersControllers.followTag);
router.delete("/users/:userId/tags/:tagId", jwtMiddleware, UsersControllers.unfollowTag);
router.post("/users/:userId/products/:productId", jwtMiddleware,  UsersControllers.followProduct);
router.delete("/users/:userId/products/:productId", jwtMiddleware, UsersControllers.unfollowProduct);
router.get("/users/:userId", UsersControllers.get);
router.patch("/users/:userId", jwtMiddleware, UsersControllers.update);

router.get("/shops/:shopId", ShopsControllers.get);
router.patch("/shops/:shopId", jwtMiddleware, ShopsControllers.update);
router.get("/shops", ShopsControllers.list);
router.post("/shops", jwtMiddleware, ShopsControllers.create);

router.get("/tags/:tagId", TagControllers.show);
router.get("/trending", jwtMiddleware2, TagControllers.userHome);

router.get("/search", SearchControllers.searchByText);
router.post("/search", upload.single("photo"), SearchControllers.searchByImage);

router.use("/me", jwtMiddleware, UsersControllers.redirMe);

router.get("/initialize", InitializeControllers.confirm);
router.post("/initialize", InitializeControllers.action);

router.get("/jwt-test", jwtMiddleware, JwtTestController);

export default router;