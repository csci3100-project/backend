import { Response, Request, NextFunction } from "express";

export default function(req: Request, res: Response, next: NextFunction) {
	res.send(req.user);
}