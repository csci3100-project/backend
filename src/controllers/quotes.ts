import ProductChildHandler from "./productChildHandler";

const productChildHandler = new ProductChildHandler("quotes");

/**
 * Update the quote information
 * It uses the update function in ProductChildHandler
 * @requires ProductChildHandler
 */
export const update = productChildHandler.update;

/**
 * Create the quote information
 * It uses the create function in ProductChildHandler
 * @requires ProductChildHandler
 */
export const create = productChildHandler.create;