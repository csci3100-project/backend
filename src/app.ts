import express from "express";
import bodyParser from "body-parser";
import logger from "morgan";
import dotenv from "dotenv";
import path from "path";
import mongoose from "mongoose";
import expressAsyncErrors from "express-async-errors";

import controllers from "./controllers";

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config();

// API keys and Passport configuration

// Create Express server
expressAsyncErrors.toString(); // hack to circumvent lazy loading
const app = express();

// Connect to MongoDB
const mongoUrl = process.env.MONGOLAB_URI;
(<any>mongoose).Promise = global.Promise;
mongoose.connect(mongoUrl).then(() => {
		console.log("Mongoose is now ready");
	},
).catch((err: any) => {
	console.error("MongoDB connection error. Please make sure MongoDB is running.", err);
	console.error("Connection URI: ", mongoUrl);
	console.error("Exiting in 5 seconds");
	setTimeout(function() {
		process.exit(1);
	}, 5000);
});

// Express configuration
app.set("port", process.env.PORT || 3000);
app.use(logger("dev"));
app.use("/api", bodyParser.json());
app.use("/api", controllers);
app.use(
	express.static(path.join(__dirname, "public"))
);
app.use("/api", function castValidationErrorTo400(err, req, res, next) {
	if (err instanceof mongoose.Error && err.name === "ValidationError") { // workaround for typescript error
		res.status(400);
	}
	next(err);
});
app.use("/uploads", express.static("uploads", {dotfiles: "allow"}));

export default app;