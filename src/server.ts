import errorHandler from "errorhandler";
import proxy from "http-proxy-middleware";

import app from "./app";

// HTML gets proxied to vue-cli or Webpack or whatever
app.use("/", proxy({target: "http://localhost:8080", changeOrigin: true}));

/**
 * Error Handler. Provides full stack - remove for production
 */
app.use(errorHandler());

/**
 * Start Express server.
 */
const server = app.listen(process.env.PORT || 3000, () => {
	console.log(
		"  App is running at http://localhost:%d in %s mode",
		app.get("port"),
		app.get("env")
	);
	console.log("  Press CTRL-C to stop\n");
});

export default server;
