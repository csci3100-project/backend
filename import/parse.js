const fs = require("fs");
const parse = require("csv-parse/lib/sync");

const csv = fs.readFileSync("google.csv", {encoding: "utf-8"});

let records = parse(csv);

records = records.map((record) => {
	const quotes = [{
		price: record[11],
		date: new Date(record[12]),
	}];
	if (record[18]) {
		quotes.push({
			price: record[18],
			date: new Date(record[19]),
		});
	}
	return {
		name: record[2],
		ean: record[1],
		link: record[3],
		quotes: quotes,
		comments: [record[6]],
		tags: record[4].split(",").map((tag) => tag.trim()),
		photos: [record[5]],
	};
});

console.log(records);
records.forEach(record => {
	console.log(record.quotes);
});