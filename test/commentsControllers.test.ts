import request from "supertest";
import app from "../src/app";
import mongoose from "mongoose";
import { pick, clone } from "lodash";
import { JWT_VALID, JWT_JOHNNY2 } from "./constants";
import { default as Product, ProductModel } from "../src/models/Product";
import { default as Shop, ShopModel } from "../src/models/Shop";
import { default as User, UserModel } from "../src/models/User";
import sleep from "await-sleep";

app.length;

describe("/comments REST endpoints", () => {
	let shop: ShopModel;
	let user: UserModel;
	let product: ProductModel;
	const ean = "fake123" + Math.floor(Math.random() * 999999);
	let comment;

	beforeAll(async () => {
		shop = await Shop.create({
			name: "testing shop",
			address: "dummy sha tin",
		});
		user = await User.create({
			name: "Johnny2 Lo",
			email: "johnny2@isaac.pw",
			jwtClaim_sub: "johnny2@isaac.pw",
			jwtClaim_iss: "https://csci3100.isaac.pw",
		});
		comment =  {
			content: "Somewhere in Shatin",
			submitter: user._id,
		};
		product = await Product.create({
			name: "testing product",
			ean: ean,
			comments: [comment],
		});
	});

	afterAll(async () => {
		await Promise.all([shop.remove(), user.remove(), product.remove()]);
	});

	it("rejects unauthrozied POST create requests", async () => {
		const res1 = await request(app)
							.post(`/api/products/${ean}/comments`)
							.set("Accept", "application/json")
							.send(comment)
							.expect(401);
	});

	it("rejects unauthrozied PATCH update requests", async () => {
		const res1 = await request(app)
							.patch(`/api/products/${ean}/comments/344556`)
							.set("Accept", "application/json")
							.send(comment)
							.expect(401);
	});

	it("allows authrozied POST create requests", async () => {
		const res1 = await request(app)
							.post(`/api/products/${ean}/comments`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_JOHNNY2}`)
							.send(comment)
							.expect(200);

		const comment2 = clone(comment);
		delete comment2.submitter;

		expect(res1.body).toMatchObject(comment2);
		expect(res1.body).toHaveProperty("submitter");
	});

	it("allows authrozied PATCH update requests", async () => {
		const comment2 = clone(comment);
		delete comment2.submitter;
		comment2.content = "content";

		const res1 = await request(app)
							.patch(`/api/products/${ean}/comments/${product.comments[0].id}`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_JOHNNY2}`)
							.send(comment2)
							.expect(200);

		expect(res1.body).toMatchObject(comment2);
		expect(res1.body).toHaveProperty("submitter");
	});

	it("replies 404 to non-existent products", async () => {
		const res1 = await request(app)
							.patch(`/api/products/abfcdcdd/comments/1234`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.send(comment)
							.expect(404);
	});

	it("replies 404 to non-existent comments", async () => {
		const res1 = await request(app)
							.patch(`/api/products/${ean}/comments/1234abc`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.send(comment)
							.expect(404);
	});

	it("replies 403 to comments not submitted by the user", async () => {
		const comment2 = clone(comment);
		comment2.content = "4030.abcd";
		const res1 = await request(app)
							.patch(`/api/products/${ean}/comments/${product.comments[0].id}`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.send(comment)
							.expect(403);
	});
});
