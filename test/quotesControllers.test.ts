import request from "supertest";
import app from "../src/app";
import mongoose from "mongoose";
import { pick, clone } from "lodash";
import { JWT_VALID, JWT_JOHNNY2 } from "./constants";
import { default as Product, ProductModel } from "../src/models/Product";
import { default as Shop, ShopModel } from "../src/models/Shop";
import { default as User, UserModel } from "../src/models/User";
import sleep from "await-sleep";

app.length;

describe("/quotes REST endpoints", () => {
	let shop: ShopModel;
	let user: UserModel;
	let product: ProductModel;
	const ean = "fake123" + Math.floor(Math.random() * 999999);
	let quote;

	beforeAll(async () => {
		shop = await Shop.create({
			name: "testing shop",
			address: "dummy sha tin",
		});
		user = await User.create({
			name: "Johnny2 Lo",
			email: "johnny2@isaac.pw",
			jwtClaim_sub: "johnny2@isaac.pw",
			jwtClaim_iss: "https://csci3100.isaac.pw",
		});
		quote =  {
			price: "33.33",
			comment: "Somewhere in Shatin",
			shop: shop.id,
			submitter: user._id,
		};
		product = await Product.create({
			name: "testing product",
			ean: ean,
			quotes: [quote],
		});
	});

	afterAll(async () => {
		await Promise.all([shop.remove(), user.remove(), product.remove()]);
	});

	it("rejects unauthrozied POST create requests", async () => {
		const res1 = await request(app)
							.post(`/api/products/${ean}/quotes`)
							.set("Accept", "application/json")
							.send(quote)
							.expect(401);
	});

	it("rejects unauthrozied PATCH update requests", async () => {
		const res1 = await request(app)
							.patch(`/api/products/${ean}/quotes/344556`)
							.set("Accept", "application/json")
							.send(quote)
							.expect(401);
	});

	it("allows authrozied POST create requests", async () => {
		const res1 = await request(app)
							.post(`/api/products/${ean}/quotes`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_JOHNNY2}`)
							.send(quote)
							.expect(200);

		const quote2 = clone(quote);
		delete quote2.submitter;

		expect(res1.body).toMatchObject(quote2);
		expect(res1.body).toHaveProperty("submitter");
	});

	it("rejects mal-formatted POST create requests", async () => {
		const quote2 = clone(quote);
		delete quote2.submitter;
		delete quote2.price;

		const res1 = await request(app)
							.post(`/api/products/${ean}/quotes`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_JOHNNY2}`)
							.send(quote2)
							.expect(400);
	});

	it("allows authrozied PATCH update requests", async () => {
		const quote2 = clone(quote);
		delete quote2.submitter;
		quote2.price = "4030.abcd";

		const res1 = await request(app)
							.patch(`/api/products/${ean}/quotes/${product.quotes[0].id}`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_JOHNNY2}`)
							.send(quote2)
							.expect(200);

		expect(res1.body).toMatchObject(quote2);
		expect(res1.body).toHaveProperty("submitter");
	});

	it("replies 404 to non-existent products", async () => {
		const res1 = await request(app)
							.patch(`/api/products/abfcdcdd/quotes/1234`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.send(quote)
							.expect(404);
	});

	it("replies 404 to non-existent quotes", async () => {
		const res1 = await request(app)
							.patch(`/api/products/${ean}/quotes/1234abc`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.send(quote)
							.expect(404);
	});

	it("replies 403 to quotes not submitted by the user", async () => {
		const quote2 = clone(quote);
		quote2.price = "4030.abcd";
		const res1 = await request(app)
							.patch(`/api/products/${ean}/quotes/${product.quotes[0].id}`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.send(quote)
							.expect(403);
	});
});
