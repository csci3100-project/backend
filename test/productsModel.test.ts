import app from "../src/app";
import Product from "../src/models/Product";
import User from "../src/models/User";
import Shop from "../src/models/Shop";

app.toString();

describe("the Product model", () => {
	let user1, user2, shop1, shop2, product, ean: string;

	beforeAll(async () => {
		user1 = await User.create({
			name: "Johnny Lo",
			email: "johnny@isaac.pw",
			jwtClaim_sub: "testForProduct" + Math.floor(Math.random() * 999999),
			jwtClaim_iss: "https://csci3100.isaac.pw",
		});
		user2 = await User.create({
			name: "Johnny2 Lo",
			email: "johnny2@isaac.pw",
			jwtClaim_sub: "testForProduct (2)" + Math.floor(Math.random() * 999999),
			jwtClaim_iss: "https://csci3100.isaac.pw",
		});
		shop1 = await Shop.create({
			name: "testing shop",
			address: "dummy sha tin",
		});
		shop2 = await Shop.create({
			name: "shop 2",
			address: "haha sha tin",
		});
		const quote1 =  {
			price: "33.33",
			comment: "Somewhere in Shatin",
			shop: shop1._id,
			submitter: user1._id,
		};
		const quote2 =  {
			price: "45.67",
			comment: "Test in Shatin",
			shop: shop2._id,
			submitter: user2._id,
		};
		ean = "fake 123" + Math.floor(Math.random() * 99999999);
		product = await Product.create({
			name: "testing product",
			ean: ean,
			quotes: [quote1, quote2],
			comments: [{
				submitter: user1._id,
				content: "haha!",
			}],
		});
	});

	afterAll(async () => {
		await Promise.all([user1.remove(), user2.remove(), shop1.remove(), shop2.remove(), product.remove()]);
	});

	it("has a ID getter", async () => {
		const product = await Product.create({
			name: "testing product",
			ean: "fake",
		});
		expect(typeof product.id).toEqual("string");
		expect(product.id).toEqual(product._id.toString());
		await expect(product.remove()).resolves.toBeTruthy();
	});
	it("removes __v and id from JSON", async () => {
		const product = await Product.create({
			name: "testing product",
			ean: "fake",
		});
		const json = product.toJSON();
		expect(json.__v).toBeUndefined();
		expect(json._id).toBeUndefined();
		expect(json.id).toBeUndefined();
		expect(json.ean).toEqual("fake");
		await expect(product.remove()).resolves.toBeTruthy();
	});
	it("finds product with EAN", async () => {
		const ean = "fake 123" + Math.floor(Math.random() * 999999);
		const product = await Product.create({
			name: "testing product",
			ean: ean,
		});
		const product2 = await Product.findByEan(ean);
		expect(product2.equals(product)).toBeTruthy();
		await expect(product.remove()).resolves.toBeTruthy();
	});
	it("returns all of a particular user's previous comments", async () => {
		const product_related = await Product.commentsBy(user1.id);

		expect(product_related).toHaveLength(1);
		expect(product_related[0].comments).toHaveLength(1);
		expect(product_related[0]).toMatchObject({
			name: "testing product",
			ean: ean,
		});
		expect(product_related[0].comments[0]).toMatchObject({
			content: "haha!",
		});
	});

	it("returns all of a particular user's previous quotes", async () => {
		const product_related = await Product.quotesBy(user1.id);

		expect(product_related).toHaveLength(1);
		expect(product_related[0].quotes).toHaveLength(1);
		expect(product_related[0]).toMatchObject({
			name: "testing product",
			ean: ean,
		});
		expect(product_related[0].quotes[0]).toMatchObject({
			price: "33.33",
			comment: "Somewhere in Shatin",
		});
	});

	it("returns all of a particular shop's previous quotes", async () => {
		const product_related = await Product.quotesFor(shop2.id);

		expect(product_related).toHaveLength(1);
		expect(product_related[0].quotes).toHaveLength(1);
		expect(product_related[0]).toMatchObject({
			name: "testing product",
			ean: ean,
		});
		expect(product_related[0].quotes[0]).toMatchObject({
			price: "45.67",
			comment: "Test in Shatin",
		});
	});
});