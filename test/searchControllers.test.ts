import request from "supertest";
import app from "../src/app";
import Product from "../src/models/Product";
import sleep from "await-sleep";

app.length; Product.length;

let product, ean;

describe("search REST endpoints", () => {
	beforeAll(async function() { // wait for fuse to load
		ean = "test for search: " + Math.floor(Math.random() * 99999999);
		product = await Product.create({
			name: "ritz cheese cracker",
			ean: ean,
		});
		await sleep(3000);
	}, 7000);

	afterAll(async function() {
		await product.remove();
	});

	it("allows people to search by text", async function() {
		const res1 = await request(app)
							.get("/api/search?keyword=ritz+cheese")
							.set("Accept", "application/json")
							.redirects(5)
							.expect(200);

		await sleep(1000);
		expect(res1.body).not.toHaveLength(0);
		expect(res1.body.filter((product) => product.ean === ean)).toHaveLength(1);
	});

	it("allows people to search by image", async function() {
		const res1 = await request(app)
							.post("/api/search")
							.attach("photo", "test/one.jpg")
							.set("Accept", "application/json")
							.redirects(5)
							.expect(200);

							await sleep(1000);
		expect(res1.body).not.toHaveLength(0);
		expect(res1.body.filter((product) => product.ean === ean)).toHaveLength(1);
	}, 12000);

	it("rejects empty GET requests", async function() {
		const res1 = await request(app)
							.get("/api/search?test=true")
							.set("Accept", "application/json")
							.redirects(5)
							.expect(400);
	});

	it("rejects empty POST requests", async function() {
		const res1 = await request(app)
							.post("/api/search")
							.set("Accept", "application/json")
							.redirects(5)
							.expect(400);
	});
});