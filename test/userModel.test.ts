import app from "../src/app";
import { pick } from "lodash";
import mongoose from "mongoose";

app.length; const User: any = mongoose.model("User");

describe("User schema validations", () => {
	it("rejects model with empty name", () => {
		const user = new User({
			email: "johndoe@example.com",
			jwtClaim_iss: "johndoe@example.com",
			jwtClaim_sub: "johndoe@example.com",
		});
		expect(user.save()).rejects.toThrow("validation");
	});
	it("rejects model with empty jwtSub", () => {
		const user = new User({
			email: "johndoe@example.com",
			name: "johndoe@example.com",
			jwtClaim_iss: "johndoe@example.com",
		});
		expect(user.save()).rejects.toThrow("validation");
	});
	it("rejects model with empty jwtIss", () => {
		const user = new User({
			email: "johndoe@example.com",
			name: "johndoe@example.com",
			jwtClaim_sub: "johndoe@example.com",
		});
		expect(user.save()).rejects.toThrow("validation");
	});
	it("rejects extra field", async function() {
		const user = User.create({
			name: "John Doe",
			email: "johndoe@example.com",
			jwtClaim_sub: "johndoe new@example.com",
			jwtClaim_iss: "Google (fake)",
			extraField: true,
		});
		await expect(user).rejects.toThrow("not in schema");
	});
	it("persists new docs", async function() {
		const user = await new User({
			name: "John Doe",
			email: "johndoe@example.com",
			jwtClaim_sub: "johndoe new@example.com",
			jwtClaim_iss: "Google (fake)",
		}).save();
		await expect(user).toMatchObject({
			name: "John Doe",
			email: "johndoe@example.com",
			jwtClaim_iss: "Google (fake)",
			jwtClaim_sub: "johndoe new@example.com",
		});
		await user.remove();
	});
});

describe("JWT static functions", function() {
	beforeEach(() => {
		app.length; // to overcome lazy loading
	});
	it("accepts new users", async function() {
		const sub = "John.Doe " + Math.random() * 9999999;
		const jwtClaim = {iss: "Google (fake3)", sub: sub, email: "johndoe@example.com", name: "John Doe"};
		const user = await User.authenticateByJwt(jwtClaim);
		await expect(user).toMatchObject({
			name: "John Doe",
			email: "johndoe@example.com",
			jwtClaim_iss: "Google (fake3)",
			jwtClaim_sub: sub,
		});
		await user.remove();
	});
	it("accepts new users with no name", async function() {
		const sub = "John.Doe " + Math.random() * 9999999;
		const jwtClaim = {iss: "Google (fake3)", sub: sub, email: "janedoe@example.com"};
		const user = await User.authenticateByJwt(jwtClaim);
		await expect(user).toMatchObject({
			name: "janedoe",
			email: "janedoe@example.com",
			jwtClaim_iss: "Google (fake3)",
			jwtClaim_sub: sub,
		});
		await user.remove();
	});
	it("returns existing users", async function() {
		const sub = "John.Doe " + Math.random() * 9999999;
		const jwtClaim = {iss: "Google (fake3)", sub: sub, email: "johndoe@example.com", name: "John Doe"};
		const user = await User.authenticateByJwt(jwtClaim);
		expect(user).toMatchObject({
			name: "John Doe",
			email: "johndoe@example.com",
			jwtClaim_iss: "Google (fake3)",
			jwtClaim_sub: sub,
		});
		const user2 = await User.authenticateByJwt(jwtClaim);
		await expect(pick(user2, ["id", "createdAt", "updatedAt"])).toEqual(pick(user, ["id", "createdAt", "updatedAt"]));
		await user2.remove();
	});

	it("generates a random name for crazily basic JWT", async function() {
		const jwtClaim = {
			"iss": "https://csci3100.isaac.pw",
			"iat": 1520606746,
			"exp": 1552142746,
			"aud": "https://csci3100.isaac.pw",
			"sub": "no email, no name - " + Math.random() * 9999999,
		};
		const user = await User.authenticateByJwt(jwtClaim);
		expect(user.email).toBeFalsy();
		expect(user.name).toBeTruthy();
		await user.remove();
	});
});