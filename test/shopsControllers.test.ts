import request from "supertest";
import app from "../src/app";
import mongoose from "mongoose";
import { pick, clone } from "lodash";
import { JWT_VALID } from "./constants";
import Shop from "../src/models/Shop";

app.length;

describe("/api/shops REST endpoints", () => {
	it("/ lists some form of array", async () => {
		const res1 = await request(app)
							.get("/api/shops")
							.set("Accept", "application/json")
							.expect(200);
		expect(res1.body).toBeInstanceOf(Array);
	});

	it("shows shops of a specific chain", async () => {
		const res1 = await request(app)
							.get("/api/shops")
							.query({chain: "test"})
							.set("Accept", "application/json")
							.expect(200);
		expect(res1.body).toBeInstanceOf(Array);
	});

	it("rejects unauthrozied POST create requests", async () => {
		const shop = {
			name: "Testing Shop",
			address: "Somewhere in Shatin",
		};
		const res1 = await request(app)
							.post("/api/shops/")
							.set("Accept", "application/json")
							.send(shop)
							.expect(401);
	});

	it("rejects unauthrozied PATCH update requests", async () => {
		const shop = {
			name: "Testing Shop",
			address: "Somewhere in Shatin",
		};
		const res1 = await request(app)
							.patch("/api/shops/12345")
							.set("Accept", "application/json")
							.send(shop)
							.expect(401);
	});

	it("creates new shops through POST", async () => {
		const shop = {
			name: "Testing Shop",
			address: "Somewhere in Shatin",
		};
		const res1 = await request(app)
							.post("/api/shops/")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.send(shop)
							.expect(200);
		expect(res1.body).toMatchObject(shop);
		expect(res1.body).toHaveProperty("id");
		await expect(Shop.remove({ _id: res1.body.id })).resolves.toBeTruthy();
	});

	it("allows people to update shop info through PATCH", async () => {
		const shop = new Shop({
			name: "Testing Shop",
			address: "Somewhere in Shatin",
		});
		await shop.save();

		try {
			const old = (await request(app)
							.get("/api/shops/" + shop.id)
							.set("Accept", "application/json")
							.expect(200)).body;

			const new1 = clone(old);
			new1.name = "This is test1";
			const res1 = await request(app)
							.patch("/api/shops/" + shop.id)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.send(new1)
							.expect(200);

			expect(res1.body).toMatchObject(pick(new1, ["id", "name", "address"]));
			expect(res1.body).toHaveProperty("createdAt");
			expect(res1.body).toHaveProperty("updatedAt");
		} catch (e) {
			expect(shop.remove()).resolves.toBeTruthy();
			throw e;
		}
		expect(shop.remove()).resolves.toBeTruthy();
	});
});
