import request from "supertest";
import app from "../src/app";
import { clone } from "lodash";
import sleep from "await-sleep";

describe("non-existent routes on non-REST router", () => {
	it("returns 404", (done) => {
		request(app).get("/random-url")
			.expect(404, done);
	});
});

describe("non-existent routes on REST router", () => {
	it("returns 404", (done) => {
		request(app).get("/api/random-url")
			.expect(404, done);
	});
});