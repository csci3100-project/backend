import request from "supertest";
import app from "../src/app";
import { JWT_VALID } from "./constants";

describe("/initialize interface", () => {
	it("returns a form when GET", async () => {
		const res1 = await request(app).get("/api/initialize")
			.expect(200);
		expect(res1.text).toEqual(expect.stringContaining("<form"));
	});

	it("reset the system by POST", async () => {
		const res1 = await request(app).post("/api/initialize")
			.expect(200);
	}, 10000);
});
