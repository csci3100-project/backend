import request from "supertest";
import app from "../src/app";
import mongoose from "mongoose";
import { pick, clone, endsWith, difference, last } from "lodash";
import { JWT_VALID } from "./constants";
import Product, { ProductModel } from "../src/models/Product";
import { default as User, UserModel } from "../src/models/User";
import { default as Shop, ShopModel } from "../src/models/Shop";

app.length;

describe("/products REST endpoints", () => {
	let product: ProductModel, ean: String, user1: UserModel, user2: UserModel, shop1: ShopModel;

	beforeAll(async () => {
		user1 = await User.create({
			name: "Test User 2 for productsControllers.test.ts",
			email: "productsControllers@isaac.pw",
			jwtClaim_sub: "productsControllers@isaac.pw",
			jwtClaim_iss: "https://csci3100.isaac.pw",
		});
		user2 = await User.create({
			name: "Test User 2 for productsControllers.test.ts",
			email: "productsControllers2@isaac.pw",
			jwtClaim_sub: "productsControllers2@isaac.pw",
			jwtClaim_iss: "https://csci3100.isaac.pw",
		});
		shop1 = await Shop.create({
			name: "testing shop",
			address: "dummy sha tin",
		});
		ean = "test1234" + Math.floor(Math.random() * 9999999);
		product = await Product.create({
			name: "Testing Product",
			ean: ean,
			comments: [{
				content: "cool!",
				submitter: user1._id,
			}, {
				content: "not good!",
				submitter: user2._id,
			}],
			quotes: [],
		});

	}, 20000);

	afterAll(async () => {
		await Promise.all([product.remove(), user1.remove(), user2.remove(), shop1.remove()]);
	});

	it("/ lists some form of array", async () => {
		const res1 = await request(app)
							.get("/api/products/")
							.set("Accept", "application/json")
							.expect(200);
		expect(res1.body).toBeInstanceOf(Array);
	});

	it("rejects unauthrozied POST create requests", async () => {
		const product = {
			name: "Testing Product",
			ean: "is this road?",
		};
		const res1 = await request(app)
							.post("/api/products/")
							.set("Accept", "application/json")
							.send(product)
							.expect(401);
	});

	it("rejects unauthrozied PATCH update requests", async () => {
		const product = {
			name: "Testing Product",
			ean: "is this road?",
		};
		const res1 = await request(app)
							.patch("/api/products/12345")
							.set("Accept", "application/json")
							.send(product)
							.expect(401);
	});

	it("creates new products through POST", async () => {
		const product = {
			name: "Testing Product",
			ean: "test1234" + Math.floor(Math.random() * 999123),
		};
		const res1 = await request(app)
							.post("/api/products/")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.send(product)
							.expect(200);
		expect(res1.body).toMatchObject(product);
		expect(res1.body).toHaveProperty("ean");
		await expect(Product.remove({ ean: res1.body.ean })).resolves.toBeTruthy();
	});

	it("creates new products with photos through POST", async () => {
		const product = {
			name: "Testing Product",
			ean: "test1234" + Math.floor(Math.random() * 999123),
		};
		const res1 = await request(app)
							.post("/api/products/")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.field("name", product.name)
							.field("ean", product.ean)
							.attach("photos", "LICENSE")
							.attach("photos", "package.json")
							.expect(200);
		expect(res1.body).toMatchObject(product);
		expect(res1.body).toHaveProperty("ean");
		expect(res1.body.photos).toHaveLength(2);
		expect(difference(["json", "LICENSE"], res1.body.photos.map((photo) => last(photo.split("."))))).toHaveLength(0);

		for (const photo of res1.body.photos) {
			await request(app).get(photo).expect(200);
		}
		await expect(Product.remove({ ean: res1.body.ean })).resolves.toBeTruthy();
	});

	it("allows people to update product info through PATCH", async () => {
		const product = new Product({
			name: "Testing Product",
			ean: "test1234" + Math.floor(Math.random() * 999123),
		});
		await product.save();

		try {
			const old = (await request(app)
							.get("/api/products/" + product.ean)
							.set("Accept", "application/json")
							.expect(200)).body;

			const new1 = clone(old);
			new1.name = "This is test1";
			const res1 = await request(app)
							.patch("/api/products/" + product.ean)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.send(new1)
							.expect(200);

			expect(res1.body).toMatchObject(pick(new1, ["id", "ean"]));
			expect(res1.body).toHaveProperty("createdAt");
			expect(res1.body).toHaveProperty("updatedAt");
		} catch (e) {
			expect(product.remove()).resolves.toBeTruthy();
			throw e;
		}
		expect(product.remove()).resolves.toBeTruthy();
	});

	it("allows people to update product info with new pics through PATCH", async () => {
		const product = new Product({
			name: "Testing Product",
			ean: "test1234" + Math.floor(Math.random() * 999123),
			photos: ["/uploads/.gitignore"],
		});
		await product.save();

		try {
			const old = (await request(app)
							.get("/api/products/" + product.ean)
							.set("Accept", "application/json")
							.expect(200)).body;

			const new1 = clone(old);
			new1.name = "This is test1";
			const res1 = await request(app)
							.patch("/api/products/" + product.ean)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.field("name", new1.name)
							.attach("photos", "appveyor.yml")
							.attach("photos", "jest.config.js")
							.expect(200);

			expect(res1.body).toMatchObject(pick(new1, ["id", "ean"]));
			expect(res1.body).toHaveProperty("createdAt");
			expect(res1.body).toHaveProperty("updatedAt");
			expect(res1.body.photos).toHaveLength(3);
			expect(difference(["gitignore", "yml", "js"], res1.body.photos.map((photo) => last(photo.split("."))))).toHaveLength(0);

			for (const photo of res1.body.photos) {
				console.error(photo);
				await request(app).get(photo).expect(200);
			}
		} catch (e) {
			expect(product.remove()).resolves.toBeTruthy();
			throw e;
		}
		expect(product.remove()).resolves.toBeTruthy();
	});

	it("returns 404 on non-existent PATCH on products", async () => {
		const res1 = await request(app)
						.patch("/api/products/12345")
						.set("Accept", "application/json")
						.set("Authorization", `Bearer ${JWT_VALID}`)
						.send(product)
						.expect(404);
	});

	it("allows people to see comments by a particular person", async () => {
		const res1 = await request(app)
						.get(`/api/users/${user1.id}/comments`)
						.set("Accept", "application/json")
						.expect(200);
		expect(res1.body).toBeInstanceOf(Array);
		expect(res1.body).toHaveLength(1);
	});

	it("allows people to see quotes by a particular person", async () => {
		const res1 = await request(app)
						.get(`/api/users/${user1.id}/quotes`)
						.set("Accept", "application/json")
						.expect(200);
		expect(res1.body).toBeInstanceOf(Array);
		expect(res1.body).toHaveLength(0);
	});

	it("allows people to see quotes for a particular shop", async () => {
		const res1 = await request(app)
						.get(`/api/shops/${shop1.id}/quotes`)
						.set("Accept", "application/json")
						.expect(200);
		expect(res1.body).toBeInstanceOf(Array);
		expect(res1.body).toHaveLength(0);
	});
});
