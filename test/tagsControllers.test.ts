import request from "supertest";
import app from "../src/app";
import mongoose from "mongoose";

import { JWT_JOHNNY3 } from "./constants";

app.length; const User: any = mongoose.model("User"), Shop: any = mongoose.model("Shop"), Product: any = mongoose.model("Product");

describe("tags REST endpoints", () => {
	let user1, shop1, ean: String, tag, product1, product2, product3;

	beforeAll(async () => {
		tag = "tag" + Math.floor(Math.random() * 9999999999);

		user1 = await User.create({
			name: "Test User 2 for productsControllers.test.ts",
			email: "johnny3@isaac.pw",
			jwtClaim_sub: "johnny3",
			jwtClaim_iss: "https://csci3100.isaac.pw",
			tags: [tag],
		});

		shop1 = await Shop.create({
					name: "testing shop",
					address: "dummy sha tin",
		});
		ean = "test1234" + Math.floor(Math.random() * 9999999);
		product1 = await Product.create({
			name: "Testing Product",
			ean: ean,
			comments: [{
				content: "cool!",
				submitter: user1._id,
			}],
			quotes: [{
				price: "33.33",
				comment: "Somewhere in Shatin",
				shop: shop1.id,
				submitter: user1._id,
			}, {
				price: "11.33",
				comment: "GGGGG in Shatin",
				shop: shop1.id,
				submitter: user1._id,
			}],
		});
		product2 = await Product.create({
			name: "Testing Product",
			ean: ean + "_2",
			tags: [tag],
			quotes: [{
				price: "33.33",
				comment: "Somewhere in Shatin",
				shop: shop1.id,
				submitter: user1._id,
			}, {
				price: "11.33",
				comment: "GGGGG in Shatin",
				shop: shop1.id,
				submitter: user1._id,
			}],
		});
		product3 = await Product.create({
			name: "Testing Product",
			ean: ean + "_3",
			tags: [tag],
			quotes: [],
		});
	});

	afterAll(async function() {
		await Promise.all([user1.remove(), shop1.remove(), product1.remove(), product2.remove(), product3.remove()]);
	});

	it("sends a customized home to each user", async function() {
		const res1 = await request(app)
						.get("/api/trending")
						.set("Accept", "application/json")
						.redirects(5)
						.expect(200);

		expect(res1.body).toBeInstanceOf(Array);
		expect(res1.body).not.toHaveLength(0);
		for (const product of res1.body) {
			expect(product.quotes).toHaveLength(1);
		}
	});

	it("allows people to look at tagged products and shops", async function() {
		const res1 = await request(app)
			.get("/api/tags/" + tag)
			.set("Accept", "application/json")
			.redirects(5)
			.expect(200);

		expect(res1.body.products).toBeInstanceOf(Array);
		expect(res1.body.shops).toBeInstanceOf(Array);
		expect(res1.body.products).toHaveLength(2);
		expect(res1.body.products[0].ean).toBe(product2.ean);
	});
});