import request from "supertest";
import app from "../src/app";
import { JWT_VALID } from "./constants";

describe("/jwt-test interface", () => {
	it("should return 401 when no token is presented", async () => {
		await request(app).get("/api/jwt-test")
			.expect(401);
	});

	it("should reject expired Google token", async () => {
		await request(app)
			.get("/api/jwt-test")
			.set("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjgzOWI5MjRiYjY1OWYwNzM1OGZkZjgyODhjZTU5ZjE4OWM0MDI4ZjQifQ.eyJhenAiOiIyNDg5MTg1MTAxNTQtajRyZXNwdnBwZWQzYWc1bXAxdmg5MTVmbTY5NjRsdTYuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiIyNDg5MTg1MTAxNTQtajRyZXNwdnBwZWQzYWc1bXAxdmg5MTVmbTY5NjRsdTYuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDIzNjU0NTYzNzk5MzA3NDA2OTgiLCJhdF9oYXNoIjoieVpBVmR0Qkd5VHVhcENkTWRJODZyZyIsImV4cCI6MTUxOTc1NjM0NiwiaXNzIjoiYWNjb3VudHMuZ29vZ2xlLmNvbSIsImp0aSI6IjE0OTE5ZmVjY2EyZGNkNTY1NGJkMjIxMzQ3OGIzMTMwZmNlZmQ0NmYiLCJpYXQiOjE1MTk3NTI3NDZ9.d1xUdLXh5ieGuARZ4pQGtOc3gMbUg1vrhRp7WVc1yd5Oafe8lqqeRhQVQokMXwuzGJal5xTDedg4kyOHePyNIvW2FB4mZA_3Eh_1IUDyC-RCoSR63wPC0iMEYkYISkj0aeCJMSzcCYySaPjnWohD_eadBVVbP4xQ4oSyYiwO59qvHrhHGPBUC6OdupevHmsY7_3Y6x_2CMtNPt4qRZrNuzxI7owhTtAPHd_6lnp3O0xNL6PrBcN13FYg_j1owfGX0Yl1W2u6EdVuH28kFpGAJlRI9kgZ0n_MLtnYf3h14eu5dlJxPzlqEDI0U7J0q5Y8rJ5zY3o1__0NsrQX3Nc1CA")
			.expect(401);
	}, 10000);

	it("should reject expired Microsoft token", async () => {
		await request(app)
			.get("/api/jwt-test")
			.set("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlNTUWRoSTFjS3ZoUUVEU0p4RTJnR1lzNDBRMCJ9.eyJhdWQiOiI3ZTRkYmQyMC1lMzllLTQ5ODEtYThjMS1lZGQyNDY3OWYwZjIiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vNTI4ZjQ0NjctOTM3Mi00YTVhLThjOTQtZmEyNzAyODUyMTVkL3YyLjAiLCJpYXQiOjE1MjA0NDg2MTksIm5iZiI6MTUyMDQ0ODYxOSwiZXhwIjoxNTIwNDUyNTE5LCJhaW8iOiJBVFFBeS84R0FBQUFJYWVjY0FWY0dSMVBFek5BVlJ0MnlrcHU2eWc1eDVRdXRmUDNJdHUxbXdCVGxnTkVCRDRkb1ViUTNVVjhnMHBUIiwiYXRfaGFzaCI6ImdVTFo4T05XN1BWajFSbDRyX2c1WXciLCJuYW1lIjoiS1dBTiwgTG9uZyBJc2FhYyIsIm5vbmNlIjoiNCIsIm9pZCI6ImI4ZmY5YmZkLTI0ZGEtNDg5ZS1hNjM1LTUyMWUxNWI5YzA2NyIsInByZWZlcnJlZF91c2VybmFtZSI6IjExNTUwNzI1NTRAbGluay5jdWhrLmVkdS5oayIsInN1YiI6IkFVX05LTmY0Sm9rS2xsOXFxUzVaVTE0dHZkU21iMDNGcUV2ZlVnY2ZEYk0iLCJ0aWQiOiI1MjhmNDQ2Ny05MzcyLTRhNWEtOGM5NC1mYTI3MDI4NTIxNWQiLCJ1dGkiOiJQTFI1ZUhsaXVrS3B6UjJXLWlFUEFBIiwidmVyIjoiMi4wIn0.clcRB-iHnBSC4yIv_gASv-teIBakAK8p5q-6zxSWNL1L0o1fV-b0TwCm8WIdpPjht2kIE2JjCg9ZcckkSjve15bN1BTZdMf0lm84IUBW9w5hyzmMXgtAW3k7NG5QOSVdRdVspSVl1NBzmi_ZHOaLlgQPnGg92_9s42rkmmVxYQ8w_bycXOmh6I8J-l85-O77d16vA3TZuozo_LAg561kL8kk4opz7isShAAYnOd-uk4NY2-IZHvTT7n66VegoeFTBEywupTP6sOaM04y0-zK8twKxD3x5wM-aUOng50XDeDaZ5o7fLqabYn0ehAgoYpBXs8Oj6DY_pdkRRgeS_uP9g")
			.expect(401);
	}, 10000);

	it("should reject unknown issuer", async () => {
		const response = await request(app)
			.get("/api/jwt-test")
			.set("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhcmUgeW91IG9uOD8iLCJpYXQiOjE1MTk3OTc1NTMsImV4cCI6MTU1MTMzMzU1MywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoiSXNhYWNLd2FuVGVzdDEyMzQiLCJlbWFpbCI6Impyb2NrZXRAZXhhbXBsZS5jb20iLCJuYW1lIjoiUHJvamVjdCBBZG1pbmlzdHJhdG9yIn0.Z_whFnYRdHHZhjiMfla-kv_--3LvxsquNdojUQPa3Zs")
			.expect(401);
		expect(response.text).toContain("Unknown JWT issuer");
	});

	it("should reject unrelated Google JWT", async () => {
		const response = await request(app)
			.get("/api/jwt-test")
			.set("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjgzOWI5MjRiYjY1OWYwNzM1OGZkZjgyODhjZTU5ZjE4OWM0MDI4ZjQifQ.eyJhenAiOiIyNDg5MTg1MTAxNTQtbGZtMmxlamxhNW9iZmt1bzIzMm11NzhjOG9yMWwxMHIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiIyNDg5MTg1MTAxNTQtbGZtMmxlamxhNW9iZmt1bzIzMm11NzhjOG9yMWwxMHIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDIzNjU0NTYzNzk5MzA3NDA2OTgiLCJoZCI6ImlzYWFja3dhbi5jb20iLCJlbWFpbCI6ImlzYWFja3dhbkBpc2FhY2t3YW4uY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF0X2hhc2giOiIyZ2RlU3FHbjF0X19zMXFZekRSQmhnIiwiZXhwIjoxNTIwMDQ5NDk4LCJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwianRpIjoiYTdmNzc0YTJmY2Q5MWFjZGQxYWI1NTI4NzJjNzEzZTYzYjliNjQ3ZiIsImlhdCI6MTUyMDA0NTg5OH0.Hnh6Q88XOmkO64tjyrVgE3jFOLgbOo98X9zKumOqjtQD9L8nfCED1wUoDD82_y_ZYqz9Y_hqJDBwuM2cKWadRU5p3VldNoYhMA1tAIXD-w0V7gpA7y8xovuyx9XVaIl09nnFQ7pQ854bDz1WE9NiN09e7sW9RqYhI4LWthO0wB6KUoR73Wg51fajz0dLgcJzghx68dlx7CnFRJW-mPdNrtW_REcLSY0cDEs7Oshwwz4Qo_aFV5QTu0N57I8qcwwBQA7mTsRoaZAzn7kyj0Oiq2wBKXAw5PZHYGFsttwjSNRH_BEXJRCK-gSL8uQaZwZ8LaoMCs8VkGMvLz_uFxn19Q")
			.expect(401);
		expect(response.text).toContain("InvalidGoogleJwtError");
	});

	it("should accept self-generated HS256 tokens", async () => {
		const response = await request(app) // using promise because supertest is buggy
			.get("/api/jwt-test")
			.set("Accept", "application/json")
			.set("Authorization", "Bearer " + JWT_VALID)
			.expect(200);
		expect(response.body).toEqual({"aud": "www.example.com", "email": "jrocket@example.com", "exp": 1551333553, "iat": 1519797553, "iss": "https://csci3100.isaac.pw", "name": "Project Administrator", "sub": "IsaacKwanTest1234"});
	});
});
