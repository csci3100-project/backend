import request from "supertest";
import app from "../src/app";
import mongoose from "mongoose";
import { pick, clone } from "lodash";
import { JWT_VALID, JWT_JOHNNY } from "./constants";

app.length; const User: any = mongoose.model("User");

describe("/users REST endpoints", () => {
	it("prints User's info", async () => {
		const user = new User({
			name: "John Doe",
			email: "johndoe@example.com",
			jwtClaim_sub: "John.Doe " + Math.random() * 9999999,
			jwtClaim_iss: "Google (fake)",
		});
		await user.save();

		const res = await request(app)
							.get("/api/users/" + user.id)
							.set("Accept", "application/json")
							.expect(200);
		expect(res.body).toMatchObject(pick(user, ["id", "jwtClaim_sub", "jwtClaim_iss"]));
		expect(res.body).toHaveProperty("createdAt");
		expect(res.body).toHaveProperty("updatedAt");
	});

	it("rejects unauthrozied updates", async () => {
		const user = new User({
			name: "Johnny Lo",
			email: "johnny@isaac.pw",
			jwtClaim_sub: "Johnny222 Lo",
			jwtClaim_iss: "https://csci3100.isaac.pw",
		});
		await user.save();
		try {
			const res1 = await request(app)
							.patch("/api/users/" + user.id)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.send({
								email: "just a test haha@isaac.pw",
							})
							.expect(403);
		} catch (e) {
			expect(user.remove()).resolves.toBeTruthy();
			throw e;
		}
		expect(user.remove()).resolves.toBeTruthy();
	});

	it("allows people to update their own info through PUT/PATCH", async () => {
		const user = new User({
			name: "Johnny Lo",
			email: "johnny@isaac.pw",
			jwtClaim_sub: "Johnny Lo",
			jwtClaim_iss: "https://csci3100.isaac.pw",
		});
		await user.save();

		try {
			const old = (await request(app)
							.get("/api/users/" + user.id)
							.set("Accept", "application/json")
							.expect(200)).body;

			const new1 = clone(old);
			new1.name = "This is test1";
			const res1 = await request(app)
							.patch("/api/users/" + user.id)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_JOHNNY}`)
							.send(new1)
							.expect(200);

			expect(res1.body).toMatchObject(pick(new1, ["id", "jwtClaim_sub", "jwtClaim_iss", "name", "email"]));
			expect(res1.body).toHaveProperty("createdAt");
			expect(res1.body).toHaveProperty("updatedAt");
		} catch (e) {
			expect(user.remove()).resolves.toBeTruthy();
			throw e;
		}
		expect(user.remove()).resolves.toBeTruthy();
	});
});

describe("/me REST endpoints", () => {
	let user;
	beforeAll(async function() {
		user = new User({
			name: "John Doe",
			email: "johndoe@example.com",
			jwtClaim_sub: "John.Doe " + Math.random() * 999999999,
			jwtClaim_iss: "Google (fake)",
		});
		await user.save();
	});


	afterAll(async function() {
		await user.remove();
	});

	it("redirects to my own profile", async function() {
		const res1 = await request(app)
							.get("/api/me/")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(0)
							.expect(307);

		const res2 = await request(app)
							.get("/api/me/")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1)
							.expect(200);

		expect(res2.body.jwtClaim_sub).toBe("IsaacKwanTest1234");
	});

	it("allows people to follow to tag", async function() {
		const tag = "tag" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.post("/api/me/tags/" + tag)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		const res2 = await request(app)
							.get("/api/me")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		expect(res2.body.tags).toContain(tag);
	});

	it("allows people to unfollow to tag", async function() {
		const tag = "tag" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.post("/api/me/tags/haha_" + tag)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		const res2 = await request(app)
							.get("/api/me")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		expect(res2.body.tags).toContain("haha_" + tag);

		const res3 = await request(app)
							.delete("/api/me/tags/haha_" + tag)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		const res4 = await request(app)
							.get("/api/me")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		expect(res4.body.tags).not.toContain("haha_" + tag);
	});

	it("rejects unauthorized follow requests", async function() {
		const tag = "tag" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.post(`/api/users/${user.id}/tags/${tag}`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(5)
							.expect(403);
	});

	it("rejects unauthorized unfollow requests", async function() {
		const tag = "tag" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.delete(`/api/users/${user.id}/tags/${tag}`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(5)
							.expect(403);
	});

	it("allows people to follow to tag", async function() {
		const tag = "tag" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.post("/api/me/tags/" + tag)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		const res2 = await request(app)
							.get("/api/me")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		expect(res2.body.tags).toContain(tag);
	});

	it("allows people to unfollow to tag", async function() {
		const tag = "tag" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.post("/api/me/tags/haha_" + tag)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		const res2 = await request(app)
							.get("/api/me")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		expect(res2.body.tags).toContain("haha_" + tag);

		const res3 = await request(app)
							.delete("/api/me/tags/haha_" + tag)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		const res4 = await request(app)
							.get("/api/me")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		expect(res4.body.tags).not.toContain("haha_" + tag);
	});

	it("rejects unauthorized follow requests", async function() {
		const tag = "tag" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.post(`/api/users/${user.id}/tags/${tag}`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(5)
							.expect(403);
	});

	it("rejects unauthorized unfollow requests", async function() {
		const tag = "tag" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.delete(`/api/users/${user.id}/tags/${tag}`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(5)
							.expect(403);
	});

	it("allows people to follow to product", async function() {
		const product = "product" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.post("/api/me/products/" + product)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		const res2 = await request(app)
							.get("/api/me")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		expect(res2.body.products).toContain(product);
	});

	it("allows people to unfollow to product", async function() {
		const product = "product" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.post("/api/me/products/haha_" + product)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		const res2 = await request(app)
							.get("/api/me")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		expect(res2.body.products).toContain("haha_" + product);

		const res3 = await request(app)
							.delete("/api/me/products/haha_" + product)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		const res4 = await request(app)
							.get("/api/me")
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(1);

		expect(res4.body.products).not.toContain("haha_" + product);
	});

	it("rejects unauthorized follow requests", async function() {
		const product = "product" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.post(`/api/users/${user.id}/products/${product}`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(5)
							.expect(403);
	});

	it("rejects unauthorized unfollow requests", async function() {
		const product = "product" + Math.floor(Math.random() * 9999999);

		const res1 = await request(app)
							.delete(`/api/users/${user.id}/products/${product}`)
							.set("Accept", "application/json")
							.set("Authorization", `Bearer ${JWT_VALID}`)
							.redirects(5)
							.expect(403);
	});
});