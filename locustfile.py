from locust import HttpLocust, TaskSet, task
from uuid import uuid4

auth_header = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2NzY2kzMTAwLmlzYWFjLnB3IiwiaWF0IjoxNTE5Nzk3NTUzLCJleHAiOjE1NTEzMzM1NTMsImF1ZCI6Ind3dy5leGFtcGxlLmNvbSIsInN1YiI6IklzYWFjS3dhblRlc3QxMjM0IiwiZW1haWwiOiJqcm9ja2V0QGV4YW1wbGUuY29tIiwibmFtZSI6IlByb2plY3QgQWRtaW5pc3RyYXRvciJ9.iv6mO9478B8qYHjMUxB4wZCWZ1EI60SmEu0-3TyVa_4'
headers = {"Authorization": auth_header}

class UserBehavior(TaskSet):
    def on_start(self):
        pass

    @task
    def getShops(self):
        self.client.get("/api/shops")

    @task
    def getProducts(self):
        self.client.get("/api/products")

    @task
    def getProfile(self):
        self.client.get("/api/me", headers=headers)

    @task
    def getSearch(self):
        self.client.get("/api/search?keyword=ritz+cheese")

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 9000

